<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Purifier;
use Session;
use DB;
use App\Home;
use Carbon\Carbon;

class HomeController extends Controller
{


  public function principal()
  {
    return view('home');
  }

  //public function vehiculo_gracias()
 // {
  //  return view('cotizar-vehiculo.gracias');
 // }



  public function post_cotizar(Request $request)
  {

    if ( Purifier::clean($request->nombres) == '' or Purifier::clean($request->apellidos) == '' or Purifier::clean($request->celular) == '' or Purifier::clean($request->correo) == '' or  Purifier::clean($request->modelo) == '' or Purifier::clean($request->mensaje) == '' or Purifier::clean($request->fecha_compra) == '') {
      return redirect()->back();
    }

    $objeto = new Home();
    $objeto->nombres      = $request->nombres;
    $objeto->apellidos    = $request->apellidos;
    $objeto->celular      = $request->celular;
    $objeto->correo       = $request->correo;
    $objeto->modelo       = $request->modelo;
    $objeto->mensaje      = $request->mensaje;
    $objeto->fecha_compra = $request->fecha_compra;
    $objeto->utm_source   = session('utm_source');
    $objeto->utm_medium   = session('utm_medium');
    $objeto->utm_campaign = session('utm_campaign');
    $objeto->save();


    $nombres        = Purifier::clean($request->nombres);
    $apellidos      = Purifier::clean($request->apellidos);
    $celular        = Purifier::clean($request->celular);
    $correo         = Purifier::clean($request->correo);
    $modelo         = Purifier::clean($request->modelo);
    $mensaje        = Purifier::clean($request->mensaje);
    $fecha_compra   = Purifier::clean($request->fecha_compra);
    $fuente         = session('utm_source');

    //return redirect()->route('gracias');

    /*----------- API Python------------*/

    $url_python = 'https://python-api-vw.herokuapp.com/insertp/0';


    $date = Carbon::now();

    $data =
    '&nombres='.$request->nombres.
    '&apellidos='.$request->apellidos.
    '&celular='.$request->celular.
    '&correo='.$request->correo.
    '&modelo='.$request->modelo.
    '&mensaje='.$request->mensaje.
    '&urgencia_compra='.$request->fecha_compra.
    '&utm_source='.$objeto->utm_source.
    '&utm_medium='.$objeto->utm_medium.
    '&utm_campaign='.$objeto->utm_campaign.
    '&fecha='.$date->day.'/'.$date->month.'/'.$date->year.
    '&mes='.$date->month.
    '&mes_ano='.$date->year.'/'.$date->month.
    '&ano='.$date->year;

    $ch = curl_init( $url_python );
    curl_setopt( $ch, CURLOPT_POST, 1);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt( $ch, CURLOPT_HEADER, 0);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

    $response = curl_exec( $ch );
    /*-----------------------------------*/


    /*******  Correos de envio de la empresa *********/
    #$correoDestino          = 'rudiyard@tuoferta.com.pe';
    $correoDestino          = 'jchanjan@porsche.com.pe';

    /*******  Valores que se envian al correo *********/
    $url = 'http://tuoferta.com.pe/send/porsche/cotizacion_porsche.php';

    $variables =

    '&nombres='.$nombres.
    '&apellidos='.$apellidos.
    '&celular='.$celular.
    '&correo='.$correo.
    '&modelo='.$modelo.
    '&mensaje='.$mensaje.
    '&fecha_compra='.$fecha_compra.
    '&fuente='.$fuente.
    '&correoDestino='.$correoDestino;
    #'&correoDestinoCopia='.$correoDestinoCopia.
    #'&correoDestinoCopiaDos='.$correoDestinoCopiaDos;

    $ch = curl_init( $url );
    curl_setopt( $ch, CURLOPT_POST, 1);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $variables);
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt( $ch, CURLOPT_HEADER, 0);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

    $response = curl_exec( $ch );


    return redirect()->route('gracias');
  }
//
 public function gracias(Request $request)
  {

    return view('gracias');
  }
}
