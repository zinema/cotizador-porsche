<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExportarController extends Controller
{
    
    public function leadcoti(){

           $datos = \DB::table('cotizaciones')           
            ->orderBy('created_at','desc')
            ->select('nombres', 'apellidos', 'celular', 'correo', 'modelo', 'mensaje', 'fecha_compra', 'created_at', 'utm_source', 'utm_medium', 'utm_campaign')
            ->get();
            

            return $datos->toJson();

    }


}


