<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('terminos', function()
{
    return view('terminos');
})->name('terminos');

Route::get('gracias', function()
{
    return view('gracias');
})->name('gracias');

Route::get('/', 'Web\HomeController@principal')->name('home');
Route::post('/cotiza-form', 'Web\HomeController@post_cotizar');

Route::get('/api/webservices/listar_cotizaciones','Panel\ExportarController@leadcoti')->name('exportar.leadcoti');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
