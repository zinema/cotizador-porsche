@extends('layouts.master')

@section('content')
<!-- Header -->
<div class="container responsivelogo">
    <div class="row">
        <div class="col-lg-12 text-center">
            <a href="{{route('home')}}"><img src="{{asset('static/img/logo-porsche.jpg')}}" alt=""></a>
        </div>
    </div>
</div>

<!--<header>
    <div class="container">
        <div class="intro-text">
           h1 class="welcometext">¡ Gracias !</h1
        </div>
    </div>
</header>-->
@section('nuevo-js')
<script>
fbq('track', 'Lead');
</script>
@stop



<section id="contact" class="paddingbot45">
    <div class="container gracias-container">
        <div class="row">
            <div class="gracias col-sm-6 padingtop-media">
                <h1>¡Gracias!</h1>
                <p class="gracias-texto" style="font-size: 16px;">Hemos recibido su informaci&oacute;n de manera correcta y nos estaremos poniendo en contacto con usted a la brevedad.</p>
                <br><br><br>
            </div>
            <div class="imagen-gracias col-sm-6 hidden-xs">
            </div>
        </div>
    </div>
</section>
<footer >
    <div class="container container2">
        <div class="row">
            <div class="col-md-12 oiek">
                <p class="marginpapu">Contactanos</p>
                <p class="mas">+</p>
            </div>
            <div class="col-md-6" style="text-align: left;">
                <p class="displaynone">Contactanos</p>
                <div class="displayflex">
                    <i class="margin-left10 fa fa-phone" aria-hidden="true"></i><p>+51 618 5070</p>
                </div><br>
                <div class="displayflex">
                    <i class="margin-left10 fa fa-envelope-o" aria-hidden="true"></i><p>marketing@porsche.com.pe</p>
                </div>
            </div>
            <div class="col-md-6" style="text-align: left;">
                <div class="displayflex marginbottomrespo">
                    <i class="margin-left10 fa fa-map-marker" aria-hidden="true"></i><p>Porsche Center Lima - Surquillo:</p><br>
                    </div><p class="marginleft-20">Av. Domingo Orué 993, Surquillo - Lima </p>
                <div class="displayflex marginbottomrespo">
                    <i class="margin-left10 fa fa-map-marker" aria-hidden="true"></i><p>Porsche Center Lima - La Molina:</p><br>
                </div>
                <p class="marginleft-20">Av. Javier Prado Este 5507, La Molina - Lima</p>
            </div>
        </div>
    </div>
</footer>
@stop


