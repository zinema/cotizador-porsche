<footer>
    <div class="container container2">
        <div class="row">
            <div class="col-md-12 oiek">
                <p class="marginpapu">Contactanos</p>
                <p class="mas">+</p>
            </div>
            <div class="col-md-6" style="text-align: left;">
                <p class="displaynone">Contactanos</p>
                <div class="displayflex">
                    <i class="margin-left10 fa fa-phone" aria-hidden="true"></i><p>+51 618 5070</p>
                </div><br>
                <div class="displayflex">
                    <i class="margin-left10 fa fa-envelope-o" aria-hidden="true"></i><p>marketing@porsche.com.pe</p>
                </div>
            </div>
            <div class="col-md-6" style="text-align: left;">
                <div class="displayflex marginbottomrespo">
                    <i class="margin-left10 fa fa-map-marker" aria-hidden="true"></i><p>Porsche Center Lima - Surquillo:</p><br>
                    </div><p class="marginleft-20">Av. Domingo Orué 993, Surquillo - Lima </p>
                <div class="displayflex marginbottomrespo">
                    <i class="margin-left10 fa fa-map-marker" aria-hidden="true"></i><p>Porsche Center Lima - La Molina:</p><br>
                </div>
                <p class="marginleft-20">Av. Javier Prado Este 5507, La Molina - Lima</p>
            </div>
        </div>
    </div>
</footer>