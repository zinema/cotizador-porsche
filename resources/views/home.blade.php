<!--@extends('layouts.master')-->

@section('content')
<!-- Header -->

<!--<header>
    <div class="container" style="margin-top: 21px;padding-top: -60px;">
        <div class="intro-text">
        <h1 class="welcometext">Bienvenido al cotizador de Porsche</h1>
        </div>
    </div>
</header>-->
    <section id="contact" class="paddingbot45">
        <div class="container contenedor-todo">
            <div class="row">
                <div class="col-md-5 imagen hidden-xs hidden-sm">
                </div>
                <div class="col-md-7 derecha padingtop-media">
                  <div class="col-md-10 col-md-offset-1">
                    <form name="sentMessage" id="contactForm" novalidate action="{{ action('Web\HomeController@post_cotizar') }}" method="POST">
                    {{ csrf_field() }}
                        <div class="row" id="background">
                            <div class="col-lg-12 logoz text-center">
                              <a href="http://localhost/proyectos/cotizador-porsche/public"><img src="http://localhost/proyectos/cotizador-porsche/public/static/img/porsche-logo.png" alt=""></a>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nombres *" name="nombres" id="name" required="true" data-validation-required-message="Tu nombre.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Apellidos *" name="apellidos" id="name" required="true" data-validation-required-message="Tus apellidos.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="E-mail *" name="correo" id="email" required="true" data-validation-required-message="Tu E-mail.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-4" style="padding: 0";>
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <input type="number" class="form-control" placeholder="Teléfono *" name="celular" id="telefono" required="true" data-validation-required-message="Tu Teléfono.">
                                      <p class="help-block text-danger"></p>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <select class="form-control pddautohg" name="modelo"  required="true">
                                        <option value="">Modelo Porsche de su inter&eacute;s *</option>
                                        <option value="911 Carrera">911 Carrera</option>
                                        <option value="718 Boxster">718 Boxster</option>
                                        <option value="Cayman">Cayman</option>
                                        <option value="Panamera">Panamera</option>
                                        <option value="Cayenne">Cayenne</option>
                                        <option value="Macan">Macan</option>
                                      </select>
                                  </div>
                              </div>
                            </div>
                            <!--<div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control pddautohg" name="fecha_compra"  required="true">
                                      <option value="" selected="true" >Pienso comprar mi auto en: (*)</option>
                                      <option value="Menos de 1 mes" >Menos de 1 mes</option>
                                      <option value="Entre 1 y 3 meses">Entre 1 y 3 meses</option>
                                      <option value="De 6 a más">De 6 a m&aacute;s</option>
                                      <option value="No tengo fecha definida">No tengo fecha definida</option>
                                    </select>
                                </div>
                            </div>-->
                            <div class="col-md-8">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Mensaje" id="message" name="mensaje" required data-validation-required-message="Tu mensaje."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-10 contacto" style="text-align: right;">
                                <p class="textocontact">
                                Al enviar sus datos acepta los <span><a href="{{route('terminos')}}">T&eacute;rminos y condiciones</span> *</a>
                                </p>
                            </div>
                            <div class="col-xs-12 col-md-2 acepto">
                              <div class="radio">
                                <label>
                                  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                  Acepto
                                </label>
                              </div>
                            </div>
                            <div class="col-lg-12 text-center">
                              <button type="submit" class="btn btn-xl">ENVIAR</button>
                            </div>
                        </div>
                    </form>
                  </div>
                  <div class="col-md-12" id="footer">
                    <div class="col-md-12 oiek">
                        <p class="marginpapu">CONTACTANOS</p>
                    </div>
                    <div class="col-md-6 contacto1" style="text-align: left;">
                        <p class="displaynone">Contactanos</p>
                        <div class="displayflex">
                            <i class="margin-left10 fa fa-phone" aria-hidden="true"></i><p>+51 618 5070</p>
                        </div><br>
                        <div class="displayflex">
                            <i class="margin-left10 fa fa-envelope-o" aria-hidden="true"></i><p>marketing@porsche.com.pe</p>
                        </div>
                    </div>
                    <div class="col-md-6" style="text-align: left;">
                        <div class="displayflex marginbottomrespo">
                            <i class="margin-left10 fa fa-map-marker" aria-hidden="true"></i><p>Porsche Center Lima - Surquillo:</p><br>
                            </div><p class="marginleft-20">Av. Domingo Orué 993, Surquillo - Lima </p>
                        <div class="displayflex marginbottomrespo">
                            <i class="margin-left10 fa fa-map-marker" aria-hidden="true"></i><p>Porsche Center Lima - La Molina:</p><br>
                        </div>
                        <p class="marginleft-20">Av. Javier Prado Este 5507, La Molina - Lima</p>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </section>
@stop


@section('js')
<!-- <script type="text/javascript">
$(document).ready(function() {

    // process the form
    $('#contactForm').submit(function(event) {
        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'name'              : $('input[name=name]').val(),
            'email'             : $('input[name=email]').val(),
            'superheroAlias'    : $('input[name=superheroAlias]').val()
        };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'https://python-api-vw.herokuapp.com/insertp/0', // the url where we want to POST
            data        : {nombres : 'xd'}, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
                        encode          : true
        })
            // using the done promise callback
            .done(function(data) {

                // log data to the console so we can see
                console.log(data);

                // here we will handle errors and validation messages
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});
</script> -->

@stop
