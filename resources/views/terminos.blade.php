@extends('layouts.master')

@section('content')

<div class="container responsivelogo">
    <div class="row">
        <div class="col-lg-12 text-center">
            <a href="{{route('home')}}"><img src="{{asset('static/img/logo-porsche.jpg')}}" alt=""></a>
        </div>
    </div>
</div>
<header class="background-terms">
    <div class="container">
        <div class="intro-text2">
        </div>
    </div>
</header>

<section id="term" class="paddingbot45">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 padingtop-media terminos">
                <h1>T&eacute;rminos y Condiciones</h1>
                <p style="font-size: 16px;">De conformidad con la Ley N° 29733 - Ley de Protecci&oacute;n de Datos Personales, declaro que en forma libre, previa, expresa e inequ&iacute;voca, brindo mi consentimiento a EUROSHOP S.A., para que realice por plazo indefinido, el tratamiento (en cualquiera de sus modalidades) de la informaci&oacute;n y los datos personales (incluyendo nombres, apellidos, direcci&oacute;n, n&uacute;mero telef&oacute;nico, correo electr&oacute;nico, imagen, caracter&iacute;sticas de veh&iacute;culos e incidencias de servicio) m&iacute;os o de mis representados, para fines administrativos, comerciales, estad&iacute;sticos, financieros, de seguimiento de intervalos de servicio, de investigaci&oacute;n y desarrollo de productos y servicios de la marca y de difusi&oacute;n de la imagen de la misma.<br><br>

                He sido adecuadamente informado que podre revocar mi consentimiento o ejercer los derechos que la ley concede a los titulares de los datos personales, de manera gratuita enviando un correo electrónico a info@porsche.com.pe.<br>

                Queda entendido que mi informaci&oacute;n y datos personales ser&aacute;n tratados en la base de datos del emisor de este documento y podr&aacute; ser tratada por terceros encargados por la empresa, quienes se obligar&aacute;n a cumplir los est&aacute;ndares de seguridad de ley y realizar el tratamiento dentro de las finalidades descritas.
                <br><br>

                <b>CAMPA&Ntilde;A CAYENNE:</b><br>

                - Campa&ntilde;a Cayenne vigente hasta agotar stock, correspondiente a versiones Cayenne S y GTS con a&ntilde;o de fabricaci&oacute;n 2017.<br>                
                - Los precios que se detallan en la publicidad corresponden a precios base.<br>
                - Tipo de cambio referencial S/3.33.<br><br>
                </p>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container container2">
        <div class="row">
            <div class="col-md-12 oiek">
                <p class="marginpapu">Contactanos</p>
            </div>
            <div class="col-md-6" style="text-align: left;">
                <p class="displaynone">Contactanos</p>
                <div class="displayflex">
                    <i class="margin-left10 fa fa-phone" aria-hidden="true"></i><p>+51 618 5070</p>
                </div><br>
                <div class="displayflex">
                    <i class="margin-left10 fa fa-envelope-o" aria-hidden="true"></i><p>marketing@porsche.com.pe</p>
                </div>
            </div>
            <div class="col-md-6" style="text-align: left;">
                <div class="displayflex marginbottomrespo">
                    <i class="margin-left10 fa fa-map-marker" aria-hidden="true"></i><p>Porsche Center Lima - Surquillo:</p><br>
                    </div><p class="marginleft-20">Av. Domingo Orué 993, Surquillo - Lima </p>
                <div class="displayflex marginbottomrespo">
                    <i class="margin-left10 fa fa-map-marker" aria-hidden="true"></i><p>Porsche Center Lima - La Molina:</p><br>
                </div>
                <p class="marginleft-20">Av. Javier Prado Este 5507, La Molina - Lima</p>
            </div>
        </div>
    </div>
</footer>
@stop


